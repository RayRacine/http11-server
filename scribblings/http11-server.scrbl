#lang scribble/manual
@require[@for-label[http11-server
                    http11
                    racket/base]]

@title{http11-server}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@defmodule[http11-server]

@section{HTTP(S) Endpoint}

@defthing[Dispatcher (-> RequestHeader Handler)]{
A Dispatcher is a function that returns the appropriate Handler function for an HTTP(S) request based on the HTTP11 request header information. 
}

@defthing[Handler (-> Input-Port Output-Port RequestHeader Void)]{
Functionally handles an HTTP(S) request.

The input port and output port is the socket of the requesting client.  The RequestHeader is the HTTP11 standard request information, HTTP Method, URI, Headings etc which has already be read off of the input port (necessary for dispatch logic which selects the appropriate Handler to invoke.

Payload information (if it exists e.g. for a POST) has not been read from the input port and assumed to be performed by the handler from the input port.

The output port, which has nothing written to it prior to invocation of this Handler.  In other words, the Handler is completely responsible for the entire HTTP response.
}

@defstruct*[Endpoint ([hostname String]
                      [dispatcher Dispatcher]
		      [port Port]
		      [cert Server-Certificate]) #:transparent]{
The configuration information sufficient to create a HTTP(S) endpoint listener/handler.

}

@defproc[(launch-endpoint [endpoint Endpoint]) Endpoint-Process]{
Based on the provided Endpoint information launch an Endpoint capable of handling HTTP(S) requests.

The return Endpoint-Process instance is used to manage the endpoint.
}
